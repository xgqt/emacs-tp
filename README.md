# Emacs-TP

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/xgqt/emacs-tp">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/xgqt/emacs-tp/">
    </a>
    <a href="https://gitlab.com/xgqt/emacs-tp/pipelines">
        <img src="https://gitlab.com/xgqt/emacs-tp/badges/master/pipeline.svg">
    </a>
</p>


## License

Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
