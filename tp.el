;;; tp.el --- Text pieces manipulation -*- lexical-binding: t -*-


;; This file is part of tp - text pieces manipulation.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; tp is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; tp is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with tp.  If not, see <https://www.gnu.org/licenses/>.


;; Authors: Maciej Barć <xgqt@riseup.net>
;; Created: 22 Dec 2022
;; Version: 0.0.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/emacs-tp
;; Package-Requires: ((emacs "26.1"))
;; SPDX-License-Identifier: GPL-2.0-or-later



;;; Commentary:

;; Text pieces manipulation.

;; Inspired by https://github.com/liferooter/textpieces/.

;; To implement:
;;   - base64 encode/decode - `tp-decode-base64', `tp-encode-base64'
;;   - escape/unescape HTML
;;   - minify JSON - `tp-minify-json'
;;   - normalize spaces (w/(o) double-space period) - `tp-normalize-spaces'
;;   - prettify JSON - `tp-prettify-json'
;;   - sort all chars on line(s) - `tp-sort-chars'
;;   - sort all words on line(s) - `tp-sort-words'
;;   - time conversion in place - `tp-convert-date'



;;; Code:


;;;###autoload
(defun tp-escape-string (beg end)
  "Escape all strings in a region.

BEG and END specify the region to use."
  (interactive "r")
  (replace-string-in-region "\"" "\\\"" beg end))

;;;###autoload
(defun tp-unescape-string (beg end)
  "Unescape all strings in a region.

BEG and END specify the region to use."
  (interactive "r")
  (replace-string-in-region "\\\"" "\"" beg end))

;;;###autoload
(defun tp-escape-url (beg end)
  "Escape URL components in a region.

This is a simple wrapper for `url-hexify-string'.

BEG and END specify the region to use."
  (interactive "r")
  (let ((region-string
         (buffer-substring beg end)))
    (delete-region beg end)
    (insert (url-hexify-string region-string))))

;;;###autoload
(defun tp-unescape-url (beg end)
  "Unescape URL components in a region.

This is a simple wrapper for `url-unhex-string'.

BEG and END specify the region to use."
  (interactive "r")
  (let ((region-string
         (buffer-substring beg end)))
    (delete-region beg end)
    (insert (url-unhex-string region-string t))))

;;;###autoload
(defun tp-reverse-sort-lines (beg end)
  "Reverse-sort lines in a region.

This is a simple wrapper for `sort-lines'.

BEG and END specify the region to sort."
  (interactive "r")
  (sort-lines t beg end))

;;;###autoload
(defun tp-secure-hash-region (beg end)
  "Call secure hash on a region, copy the created hash to the ‘kill-ring’.

BEG and END specify the region to use."
  (interactive "r")
  (let ((region-hash
         (secure-hash (intern (completing-read "Secure hash algorithm: "
                                               (secure-hash-algorithms)))
                      (buffer-substring beg end))))
    (kill-new region-hash)
    (message "Hash \"%s\" inserted to the kill-ring." region-hash)))

;;;###autoload
(defun tp-titlecase (beg end)
  "Title-case text in region.

BEG and END specify the region to use."
  (interactive "r")
  (let ((words
         '(" a"
           " and"
           " as"
           " at"
           " be"
           " by"
           " for"
           " from"
           " in"
           " into"
           " is"
           " it"
           " of"
           " on"
           " or"
           " that"
           " the"
           " to"
           " via"
           " vs"
           " with"
           "'s"
           "'t")))
    (save-excursion
      (save-restriction
        (narrow-to-region beg end)
        (upcase-initials-region (point-min) (point-max))
        (mapc (lambda (word)
                (goto-char (point-min))
                (let ((word-regexp
                       (concat (capitalize word) " ")))
                  (while (re-search-forward word-regexp nil t)
                    (replace-match (concat word " ") t t))))
              words)))))


(provide 'tp)



;;; tp.el ends here and end the ends
